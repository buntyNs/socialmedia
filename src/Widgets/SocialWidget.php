<?php

namespace SocialMediaWidget\Widgets;

use Ceres\Widgets\Helper\BaseWidget;

class SocialWidget extends BaseWidget
{
    protected $template = "SocialMediaWidget::Widgets.SocialWidget";

    protected function getTemplateData($widgetSettings, $isPreview)
    {
       

        $checkAlert =$widgetSettings["checkAlert"]["mobile"];
        $style =$widgetSettings["style"]["mobile"];
        $animation =$widgetSettings["animation"]["mobile"];
        $btnType =$widgetSettings["btnType"]["mobile"];
        $link =$widgetSettings["link"]["mobile"];


        $checkfacebook=$widgetSettings["checkfacebook"]["mobile"];
        $checkPinterest=$widgetSettings["checkPinterest"]["mobile"];
        $checkSkype=$widgetSettings["checkSkype"]["mobile"];
        $checkGoogle=$widgetSettings["checkGoogle"]["mobile"];
        $checkLinkedin=$widgetSettings["checkLinkedin"]["mobile"];
        $checkXing=$widgetSettings["checkXing"]["mobile"];
        $checktumblr=$widgetSettings["checktumblr"]["mobile"];
        $checkreddit=$widgetSettings["checkreddit"]["mobile"];
        $checkstumbleupon=$widgetSettings["checkstumbleupon"]["mobile"];
        $checkdigg=$widgetSettings["checkdigg"]["mobile"];
        $checkFlattr=$widgetSettings["checkFlattr"]["mobile"];
        $checkbuffer=$widgetSettings["checkbuffer"]["mobile"];
        $checkVkontakte=$widgetSettings["checkVkontakte"]["mobile"];
        $checkWhatsApp = $widgetSettings["checkWhatsApp"]["mobile"];
        $checkBlogger = $widgetSettings["checkBlogger"]["mobile"];
        $checkWordpress = $widgetSettings["checkWordpress"]["mobile"];
        $checkYahoo = $widgetSettings["checkYahoo"]["mobile"];



        $facebook=$widgetSettings["facebook"]["mobile"];
        $Pinterest=$widgetSettings["pinterest"]["mobile"];
        $Skype=$widgetSettings["skype"]["mobile"];
        $Google=$widgetSettings["googleplus"]["mobile"];
        $Linkedin=$widgetSettings["linkedin"]["mobile"];
        $Xing=$widgetSettings["xing"]["mobile"];
        $tumblr=$widgetSettings["tumblr"]["mobile"];
        $reddit=$widgetSettings["reddit"]["mobile"];
        $stumbleupon=$widgetSettings["stumbleupon"]["mobile"];
        $digg=$widgetSettings["digg"]["mobile"];
        $Flattr=$widgetSettings["flattr"]["mobile"];
        $buffer=$widgetSettings["buffer"]["mobile"];
        $Vkontakte=$widgetSettings["vkontakte"]["mobile"];
        $WhatsApp=$widgetSettings["whatsApp"]["mobile"];
        $blogger=$widgetSettings["blogger"]["mobile"];
        $wordpress=$widgetSettings["wordpress"]["mobile"];
        $yahoo=$widgetSettings["yahoo"]["mobile"];



        if($checkfacebook){
            $send["facebook"] = $checkfacebook; 
        }
        if($checkPinterest){
            $send["pinterest"] = $checkPinterest; 
        }
        if($checkSkype){
            $send["skype"] = $checkSkype; 
        }
        if($checkGoogle){
            $send["googleplus"] = $checkGoogle; 
        }
        if($checkLinkedin){
            $send["linkedin"] = $checkLinkedin; 
        }
        if($checkXing){
            $send["xing"] = $checkXing; 
        }
        if($checktumblr){
            $send["tumblr"] = $checktumblr; 
        }
        if($checkstumbleupon){
            $send["stumbleupon"] = $checkstumbleupon; 
        }
        if($checkdigg){
            $send["digg"] = $checkdigg; 
        }
        if($checkBlogger){
            $send["blogger"] = $checkBlogger; 
        }
        if($checkreddit){
            $send["reddit"] = $checkreddit; 
        }

        if($checkFlattr){
            $send["flattr"] = $checkFlattr; 
        }
        if($checkbuffer){
            $send["buffer"] = $checkbuffer; 
        }
        if($checkVkontakte){
            $send["vkontakte"] = $checkVkontakte; 
        }
        if($checkWhatsApp){
            $send["whatsapp"] = $checkWhatsApp; 
        }
        if($checkWordpress){
            $send["wordpress"] = $checkWordpress; 
        }
        if($checkYahoo){
            $send["yahoo"] = $checkYahoo; 
        }
        

        if (true)
        {
            return [
                "btnArray" => $send,
                "styles" => [
                    "style" => $style,
                    "animation" => $animation,
                    "btnType" =>$btnType,
                    "checkAlert"=>$checkAlert,
                    "link" => $link
                   
                ],
                "hover" => [
                        "facebook" => $facebook,
                        "linkedin" => $Linkedin,
                        "googleplus"=> $Google,
                        "pinterest" => $Pinterest,
                        "skype" => $Skype,
                        "xing" => $Xing,//no style 3
                        "tumblr" => $tumblr,
                        "reddit" => $reddit,
                        "stumbleupon" => $stumbleupon,//no icon in style 1 and 3
                        "digg" => $digg,
                        "flattr" => $Flattr,//no icon in style 3
                        "buffer" => $buffer,//no icon in style 3
                        "vkontakte" => $Vkontakte,//no icon in style 3
                        "whatsapp" => $WhatsApp,//no icon in style 3 and 2
                        "blogger"=>$blogger,
                        "yahoo"=>$yahoo,
                        "wordpress"=>$wordpress
                    ]
            ];
        }

        return [
            "false" => false
        ];
    }
}
