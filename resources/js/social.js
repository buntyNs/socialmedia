
var data = '';
var span = document.getElementsByClassName("close")[0];


//check box functions
function checkBox() {
    var checkBox = document.getElementById("my_checkbox");
    var text = document.getElementById("text");
    if (checkBox.checked == true) {
        document.getElementById("accept").disabled = false;
    } else {
        document.getElementById("accept").disabled = true;
    }
}

//modal accept acction
function acceptAction() {
    aTag = document.getElementById('id_' + data);
    aTag.setAttribute('availability', 'true');
    modal = document.getElementById('myModal');
    modal.style.display = "none";
    document.getElementById("my_checkbox").checked = false;
    document.getElementById("accept").disabled = true;
    if (aTag.getAttribute('styleName') === "1") {
        changeStyleOne(aTag);
    }

}

//model colse action
function closetAction() {
    aTag = document.getElementById(data);
    aTag.setAttribute('availability', 'false');
    modal = document.getElementById('myModal');
    modal.style.display = "none";
    document.getElementById(data).checked = false;
    document.getElementById("my_checkbox").checked = false;
    document.getElementById("accept").disabled = true;
    if (aTag.getAttribute('styleName') === "1") {
        changeStyleTwo(aTag);
    }

}

//switch action
function switchAction(value) {
    if (value.checked) {
        if (value.getAttribute('confirm') == true) {
            modal = document.getElementById('myModal');
            modal.style.display = "block";
            data = value.getAttribute('id');//set current id to data
        } else {
            data = value.getAttribute('id');//set current id to data
            aTag = document.getElementById('id_' + data);
            aTag.setAttribute('availability', 'true');
            if (aTag.getAttribute('styleName') === "1") {
                changeStyleOne(aTag);
            }
        }

    } else {
        data = value.getAttribute('id');//set current id to data
        aTag = document.getElementById('id_' + data);
        aTag.setAttribute('availability', 'false');
        if (aTag.getAttribute('styleName') === "1") {
            changeStyleTwo(aTag);
        }

    }

}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    modal = document.getElementById('myModal');
    if (event.target == modal) {
        modal.style.display = "none";
        document.getElementById("my_checkbox").checked = false;
        document.getElementById(data).checked = false;
        document.getElementById("accept").disabled = true;
        aTag = document.getElementById('id_' + data);
        if (aTag.getAttribute('styleName') === "1") {
            changeStyleTwo(aTag);
        }
    }
}


function changeStyleOne(value) {
    value.setAttribute('class', 'stb-' + data);
}

function changeStyleTwo(value) {
    value.setAttribute('class', 'stb-' + data + '_gray');

}

//share button api
function shareAction(value) {
    if(value.getAttribute('availability') === "true"){
    id = value.getAttribute('id');
    var x = document.URL;
    switch (id) {
        case "id_facebook": window.open('https://www.facebook.com/sharer/sharer.php?u=' + x,
            'facebook-share-dialog',
            'width=800,height=600');break;
        case "id_linkedin" : window.open(
            "https://www.linkedin.com/shareArticle?mini=true&url=" + x + "&title=LinkedIn%20Developer%20Network&summary=My%20favorite%20developer%20program&source=LinkedIn",
             'window name',
             'width=800,height=600');
              break;
        case "id_xing" : window.open("https://www.xing.com/spi/shares/new?url=" + x, 'window name', 'width=800,height=600');
        break;

        case "id_tumblr":window.open("http://tumblr.com/widgets/share/tool?canonicalUrl=" + x, 'window name', 'width=800,height=600');
        break;

        case "id_reddit" : window.open("http://www.reddit.com/submit?url=" + x, 'window name', 'width=800,height=600');
        break;

        case "id_googleplus": window.open("https://plus.google.com/share?url=" + x, 'window name', 'width=800,height=600');
        break;
        case "id_pinterest": window.open("http://pinterest.com/pin/create/button/?url=" + x, 'window name', 'width=800,height=600');
        break;
      
        case "id_skype" :window.open("https://web.skype.com/share?url="+ x , 'window name', 'width=800,height=600');
        break;

        case "id_digg":window.open("http://digg.com/submit?url=" + x, 'window name', 'width=800,height=600');
        break;

        case "id_flattr":window.open("https://flattr.com/submit/auto?user_id=1&url="+ x, 'window name', 'width=800,height=600');
        break;

        case "id_buffer":window.open("https://buffer.com/add?text={title}&url="+ x, 'window name', 'width=800,height=600');
        break;

        case "id_vkontakte":window.open("http://vk.com/share.php?url="+ x, 'window name', 'width=800,height=600');
        break;

        case "id_whatsapp":window.open("http://pinterest.com/pin/create/button/?url=" + x, 'window name', 'width=800,height=600');
        break;
        
        case "id_blogger" :window.open( "https://www.blogger.com/blog-this.g?u="+ x, 'window name', 'width=800,height=600');
        break;

        case "id_yahoo" :window.open( "http://compose.mail.yahoo.com/?to={email_address}&subject={title}&body= "+ x, 'window name', 'width=800,height=600');
        break;

        case "id_wordpress":window.open( "http://www.example.com/website_where_you_installed_wordpress/wp-admin/press-this.php?u="+ x, 'window name', 'width=800,height=600');
        break;
        default : null;



    }
}
}







